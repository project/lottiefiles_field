# Lottiefiles Field

## Introduction

Lottie is a file format for vectorial animation and Lottiefiles is an
open-source and super lightweight format that is vector-based interactive,
and cross-platform. LottieFiles provides all the tools that you need to
create, edit, test and display Lottie animations.

Here is the drupal field for Lottifiles...!!! You can create the animation
and paste the json file URL to the Drupal lottiefiles fields in your content.

## Installation

[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)

## Usage

* Create lottiefile field type field under your content type.
* Create the content and add json file url on lottiefile field.
* Add your lottiefiles properties as per you wish.

## Options

As per the [Installing Modules](https://lottiefiles.com/web-player)

* Autoplay - Automatic play the animation.
* Background - Hex value or transparent
* Add Class - Optional CSS class
* Controls - Display Animation Controls: Play, Pause & Slider
* Hover - Whether to play on mouse hover.
* Player Mode - Normal or Bounce.
* Speed - Animation speed in number 1 to 5.
* Width - Width in pixel.

## Maintainers
 * Hari Venu (harivenuv) - https://www.drupal.org/u/harivenuv
