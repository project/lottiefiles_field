<?php

namespace Drupal\lottiefiles_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Render\Markup;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'lottiefiles_field' widget.
 *
 * @FieldWidget(
 *   id = "lottiefiles_field",
 *   label = @Translation("Lottiefiles Field"),
 *   field_types = {
 *     "lottiefiles_field"
 *   }
 * )
 */
class LottiefilesFieldWidget extends LinkWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'autoplay' => 0,
      'background' => 'transparent',
      'addclass' => '',
      'controls' => 0,
      'hover' => 0,
      'loop' => 0,
      'mode' => 'Normal',
      'speed' => 1,
      'selector' => '',
      'width' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Get the field settings.
    $elements = parent::settingsForm($form, $form_state);
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#attached']['library'][] = 'lottiefiles_field/lottiefiles_field.widget';
    $element['uri']['#title'] = $this->t('Lottiefile URL');
    $element['uri']['#weight'] = -20;
    $element['uri']['#required'] = FALSE;
    $element['uri']['#description'] = $this->t('This must be an external JSON file URL such as <em>@url</em>', ['@url' => 'https://assets3.lottiefiles.com/packages/lf20_1gqj80jh.json']);
    $randSelector = $this->generateRandomString(5);

    /** @var \Drupal\link\LinkItemInterface $item */
    $item = $items[$delta];
    $element += [
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $color = (isset($item->background) && $item->background != 'transparent') ? Xss::filter($item->background) : '#000000';
    $colorpicker = $this->t('Pick the background color :') . ' <input type="color" data-target="#' . 'lottie-' . $randSelector . '" name="bgcolorpicker" value="' . $color . '">';

    $element['file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('JSON File upload'),
      '#description' => $this->t('You can upload your own lottiefiles in JSON format, it will automatically generate the Lottiefile URL.'),
      '#upload_validators' => [
        'file_validate_extensions' => ['json'],
      ],
      '#upload_location' => 'public://lottiefile_field/',
    ];
    $element += [
      'autoplay' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Autoplay'),
        '#description' => $this->t('Autoplay animation on load.'),
        '#default_value' => $item->autoplay,
      ],
      'background' => [
        '#type' => 'textfield',
        '#title' => $this->t('Background'),
        '#description' => $this->t('Background color must be HEX value with # also only accepted `transparent` keyword'),
        '#default_value' => isset($item->background) ? Xss::filter($item->background) : 'transparent',
        '#element_validate' => [
          [static::class, 'colorValidate'],
        ],
        '#attributes' => [
          'id' => 'lottie-' . $randSelector,
        ],
        '#suffix' => Markup::create($colorpicker),
      ],
      'addclass' => [
        '#type' => 'textfield',
        '#title' => $this->t('Add Class'),
        '#description' => $this->t('Add CSS class.'),
        '#default_value' => $item->addclass,
      ],
      'controls' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Controls'),
        '#description' => $this->t('Show controls.'),
        '#default_value' => $item->controls,
      ],
      'hover' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Hover'),
        '#description' => $this->t('Whether to play on mouse hover.'),
        '#default_value' => $item->hover,
      ],
      'loop' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Loop'),
        '#description' => $this->t('Whether to loop animation.'),
        '#default_value' => $item->loop,
      ],
      'mode' => [
        '#type' => 'select',
        '#title' => $this->t('Mode'),
        '#options' => [
          'normal' => $this->t('Normal'),
          'bounce' => $this->t('Bounce'),
        ],
        '#description' => $this->t('Play mode.'),
        '#default_value' => $item->mode,
      ],
      'speed' => [
        '#type' => 'select',
        '#title' => $this->t('Speed'),
        '#options' => [
          1 => 1,
          2 => 2,
          3 => 3,
          4 => 4,
          5 => 5,
        ],
        '#description' => $this->t('Animation speed.'),
        '#default_value' => $item->speed,
      ],
      'selector' => [
        '#type' => 'hidden',
        '#title' => $this->t('Selector'),
        '#description' => $this->t('Unique selector identifier.'),
        '#default_value' => empty($item->selector) ? 'lottie-' . $randSelector : $item->selector,
      ],
      'width' => [
        '#type' => 'number',
        '#min' => 0,
        '#title' => $this->t('Width in pixel'),
        '#description' => $this->t('Width of the lottiefile animation.'),
        '#default_value' => empty($item->width) ? 0 : $item->width,
      ],
    ];
    return $element;
  }

  /**
   * Validate the color text field background.
   */
  public static function colorValidate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }
    if ($value == 'transparent') {
      $form_state->setValueForElement($element, 'transparent');
      return;
    }
    if (!preg_match('/^#([a-f0-9]{6})$/iD', strtolower($value))) {
      $form_state->setError($element, "Color must be a 6-digit hexadecimal value with #, suitable for CSS.");
    }
  }

  /**
   * Random String generator.
   */
  public static function generateRandomString($length) {
    return Crypt::randomBytesBase64($length);
  }

  /**
   * Form element validation handler for the 'uri' element.
   *
   * Disallows saving inaccessible or untrusted URLs.
   */
  public static function validateUriElement($element, FormStateInterface $form_state, $form) {

    $uri = static::getUserEnteredStringAsUri($element['#value']);

    // If file upload get new value.
    // Check file upload field value.
    $values = $form_state->getValues();
    if (isset($values[$element['#array_parents'][0]][$element['#array_parents'][2]]['file']['fids'][0])) {
      $fid = $values[$element['#array_parents'][0]][$element['#array_parents'][2]]['file']['fids'][0];
      $file_object = File::load($fid);
      $file_object->setPermanent();
      $file_object->save();
      $file_uri = $file_object->getFileUri();
      $uri = Url::fromUri(file_create_url($file_uri))->toString();
      $uri = static::getUserEnteredStringAsUri($uri);
    }

    $form_state
      ->setValueForElement($element, $uri);

    // If getUserEnteredStringAsUri() mapped the entered value to a 'internal:'
    // URI , ensure the raw value begins with '/', '?' or '#'.
    // @todo '<front>' is valid input for BC reasons, may be removed by
    //   https://www.drupal.org/node/2421941
    if (parse_url($uri, PHP_URL_SCHEME) === 'internal' && !in_array($element['#value'][0], [
      '/',
      '?',
      '#',
    ], TRUE) && substr($element['#value'], 0, 7) !== '<front>') {
      $form_state
        ->setError($element, t('Manually entered paths should start with /, ? or #.'));
      return;
    }
  }

}
